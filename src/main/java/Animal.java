/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public interface Animal {

    String som() ;

}
