/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Cachorro implements Animal {

    public String som() {
        return "latir";
    }
}
