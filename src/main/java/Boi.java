/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class Boi implements Animal {

    public String som() {
        return "mugir";
    }
}
