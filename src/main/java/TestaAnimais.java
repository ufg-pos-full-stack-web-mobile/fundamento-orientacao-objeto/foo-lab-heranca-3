import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 13/05/17.
 */
public class TestaAnimais {

    public static void main(String... args) {
        List<Animal> animais = new ArrayList<Animal>(15);

        Sapo sapo = new Sapo();
        Cachorro cachorro = new Cachorro();
        Boi boi = new Boi();

        animais.add(sapo);
        animais.add(cachorro);
        animais.add(boi);

        for (Animal animal : animais) {
            System.out.println(animal.som());
        }

    }
}
